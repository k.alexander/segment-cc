## Segment-CC

[CloudCompare](https://www.cloudcompare.org/) plugin utilizing [libsegment](https://gitlab.com/k.alexander/ma) to offer semantic segmentation of point clouds from directly within
CloudCompare. The plugin does not link against libsegment but rather loads it dynamically as LibTorch is rather
large and would make packaging a bit hard.

### Building

Place in `plugins/core/Standard/` in the CloudCompare source tree and add `add_subdirectory(Segment-CC)` to the CMakeLists.txt file in the `Standard` folder.

License: AGPLv3.0 or later
