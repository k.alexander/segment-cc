/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ccStdPluginInterface.h"

class qSegment : public QObject
    , public ccStdPluginInterface {
    Q_OBJECT
    Q_INTERFACES(ccPluginInterface ccStdPluginInterface)

    Q_PLUGIN_METADATA(IID "cc.kozel.qSegment" FILE "../info.json")

public:
    explicit qSegment(QObject* parent = nullptr);

    // inherited from ccStdPluginInterface
    virtual void
    onNewSelection(const ccHObject::Container& selectedEntities) override;
    virtual QList<QAction*> getActions() override;
    virtual void registerCommands(ccCommandLineInterface* cmd) override;

    //   static ccHObject *executeRANSAC(ccPointCloud *ccPC,
    //                                   const RansacParams &params,
    //                                   bool silent = false);

protected:
    void doAction();

protected:
    QAction* m_action;
};
