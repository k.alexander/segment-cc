/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QTimer>
#include <atomic>
#include <ccPointCloud.h>
#include <memory>
#include <thread>

#include "lib.h"
#include "ui_segmentDlg.h"

class ccSegmentDlg : public QDialog
    , public Ui::SegmentDialog {
    Q_OBJECT

    ccPointCloud* m_pc {};

    std::unique_ptr<LibSegment> m_lib;

    SegmentKPConvConfig m_kpconv_config {};
    SegmentNetworkConfig m_base_settings {};

    uint8_t* m_segment_result {};
    std::atomic<bool> m_segmentation_done { false };

    QTimer m_tick_timer {};

    std::thread m_segment_thread;

    void toggleKPConvSettings(bool enabled);

public:
    explicit ccSegmentDlg(ccPointCloud* pc, QWidget* parent = nullptr);

    ~ccSegmentDlg() override
    {
        unloadLibrary();
    }

protected:
    void saveSettings();

protected slots:

    void loadLibrary();
    void unloadLibrary();

    void loadModel();
    void unloadModel();

    void startSegmentation();
    void cancelSegmentation();

    void updateTick();
};
