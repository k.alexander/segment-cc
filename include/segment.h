/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <cstdint>

#ifdef _MSC_VER
#    pragma once
#    if defined(DLLIMPORT)
#        define EXPORT __declspec(dllimport)
#    else
#        define EXPORT __declspec(dllexport)
#    endif
#else
#    define EXPORT __attribute__((visibility("default")))
#endif

// C interface

#ifdef __cplusplus
extern "C" {
#endif

typedef void* SegmentHandle;

enum SegmentComputePlatform {
    SEGMENT_COMPUTE_CPU = 0,
    SEGMENT_COMPUTE_CUDA = 1,
    SEGMENT_COMPUTE_ROCM = 2,
    SEGMENT_COMPUTE_INVALID_PLATFORM = -1
};

struct SegmentVersion {
    union {
        struct {
            uint8_t major;
            uint8_t minor;
            uint8_t patch;
        };
        uint32_t version;
    };
};

struct SegmentVersionInfo {
    SegmentVersion library_version;
    SegmentVersion torch_version;
    SegmentVersion compute_version;
    SegmentComputePlatform compute_platform;
};

enum SegmentError {
    SEGMENT_INVALID = -1,
    SEGMENT_OK,
    SEGMENT_FILE_NOT_FOUND,
    SEGMENT_MODEL_LOADING_FAILED,
    SEGMENT_POINTCLOUD_LOADING_FAILED,
    SEGMENT_DOWNSAMPLING_FAILED,
    SEGMENT_KDTREE_CREATION_FAILED,
    SEGMENT_CALIBRATION_CONVERGENCE_FAILED,
    SEGMENT_LOADING_PROJECTION_FAILED,
    SEGMENT_NO_MODEL_LOADED,
    SEGMENT_INVALID_ARGUMENT,
    SEGMENT_NOT_IMPLEMENTED,
    SEGMENT_INTERRUPTED
};

enum SegmentInfoType {
    SEGMENT_INFO_INVALID = -1,
    SEGMENT_INFO_CURRENT_POINTS, // List of points currently being processed
    SEGMENT_INFO_CURRENT_PROBS,  // List of probabilities for the pointcloud
};

struct SegmentNetworkConfig {
    /**
     * @brief Number of threads to use for fetching data (defaults to the number of cores)
     *        note that for randlanet using more than one thread isn't beneficial when
     *        using cpu inference as the inference will be the bottleneck and parallelizing
     *        the data fetching can sometimes result in more iterations being needed to
     *        process a point cloud.
     */
    int num_fetch_threads;

    /**
     * @brief Amount of times to iterate over the dataset (default: 10)
     */
    int num_iterations;

    /**
     * @brief Number of batches to take out of the dataset per iteration (default: 50)
     */
    int validation_size;

    /**
     * @brief Use the GPU for inference if available (default: true)
     */
    bool prefer_gpu_inference;

    /**
     * @brief Inference device to use (default: 0)
     */
    int inference_device;

    /**
     * @brief If true the network will provide data about the currently processed points
     *        and the probabilities for all points once every ten iterations.
     *        This does slow down the processing a bit (default: false)
     */
    uint8_t enable_live_data;
};

// Function pointer types
typedef bool (*segment_is_gpu_available_t)();
typedef void (*segment_free_network_t)(SegmentHandle);
typedef void (*segment_configure_kpconv_network_t)(SegmentHandle, const void*);
typedef void (*segment_configure_randla_network_t)(SegmentHandle, const void*);
typedef SegmentHandle (*segment_create_kpconv_network_t)(const char*);
typedef SegmentHandle (*segment_create_randla_network_t)(const char*);
typedef SegmentError (*segment_initialize_t)(SegmentHandle);
typedef SegmentError (*segment_segment_pointcloud_t)(SegmentHandle, char const*, char const*);
typedef SegmentError (*segment_segment_points_t)(SegmentHandle, float const*, uint64_t, uint8_t**);
typedef SegmentError (*segment_segment_points_with_rgb_t)(SegmentHandle, float const*, float const*, uint64_t, uint8_t**);
typedef char* (*segment_fetch_log_t)(SegmentHandle);
typedef void (*segment_free_t)(void*);
typedef void (*segment_get_version_info_t)(SegmentVersionInfo*);
typedef char* (*segment_fetch_progress_t)(SegmentHandle, float*);
typedef void* (*segment_get_info_data_t)(SegmentHandle, SegmentInfoType, uint64_t*);
typedef void (*segment_interrupt_segmentation_t)(SegmentHandle);
typedef uint8_t (*segment_is_info_data_updated_t)(SegmentHandle, SegmentInfoType);
typedef void (*segment_set_torch_seed_t)(uint64_t);

EXPORT void segment_set_torch_seed(uint64_t seed);

EXPORT bool segment_is_gpu_available();

EXPORT void segment_free_network(SegmentHandle network);

EXPORT void segment_free(void* ptr);

EXPORT void segment_get_version_info(SegmentVersionInfo* out_info);

EXPORT void* segment_get_info_data(SegmentHandle network, SegmentInfoType type, uint64_t* out_info_size);

EXPORT uint8_t segment_is_info_data_updated(SegmentHandle network, SegmentInfoType type);

/**
 * @brief Initialize the network, i.e. load the model
 * @param network The network to initialize
 * @return SegmentError::OK if successful
 */
EXPORT SegmentError segment_initialize_network(SegmentHandle network);

/**
 * @brief Segment a point cloud
 * @param network The network to use
 * @param input_cloud_path Path to the input point cloud (PLY file)
 * @param output_cloud_folder Path to the output folder
 * @return SegmentError::OK if successful
 */
EXPORT SegmentError segment_segment_pointcloud(SegmentHandle network, char const* input_cloud_path, char const* output_cloud_folder);

/**
 * @brief Segments points directly from and to memory
 * @param network The network to use
 * @param points Array of points
 * @param num_points Number of points
 * @param classifications Array of classifications, free with segment_free
 * @param count Number of classifications
 * @return SegmentError::OK if successful
 */
EXPORT SegmentError segment_segment_points(SegmentHandle network, float const* points, uint64_t num_points, uint8_t** classifications);

/**
 * @brief Segments points directly from and to memory
 * @param network The network to use
 * @param points Array of points
 * @param color Array of colors (RGB)
 * @param num_points Number of points
 * @param classifications Array of classifications, free with segment_free
 * @param count Number of classifications
 * @return SegmentError::OK if successful
 */
EXPORT SegmentError segment_segment_points_with_rgb(SegmentHandle network, float const* points, float const* color, uint64_t num_points, uint8_t** classifications);

/**
 * @brief Stop segmentation if it is currently running
 */
EXPORT void segment_interrupt_segmentation(SegmentHandle network);

/**
 * @brief Fetch latest log lines from the network
 * @return log lines, free with segment_free, or NULL if log queue is empty
 */
EXPORT char* segment_fetch_log(SegmentHandle network);

/**
 * @brief Fetch the current progress of the network
 * @param network The network to fetch the progress from
 * @param progress Pointer to a float to store the progress in
 * @return progress string, free with segment_free
 */
EXPORT char* segment_fetch_progress(SegmentHandle network, float* progress);

/* ================================ KPConv ================================ */

struct SegmentKPConvConfig {
    SegmentNetworkConfig base;

    /**
     * @brief Weighting of old predictions vs new prediction, i.e.
     * test_smooth * old + (1 - test_smooth) * new (default: 0.95)
     */
    float test_smooth;

    /**
     * @brief Percentage of the sphere radius used for masking predictions,
     * i.e. only consider predictions within input_radius * test_radious_ratio
     * (default: 0.7)
     */
    float test_radius_ratio;

    /*
        Parameters for calibration
        Before a point cloud can be segmented some paramters for KPConv have to be
        calibrated. The following parameters configure this calibration process.
        Depending on the pointcloud these parameters might need to be adjusted to
        insure that the calibration process converges.
    */

    /**
     * @brief Expected batch size order of magnitude (default: 100000)
     */
    int expected_N;

    /*
        Higher means faster but can also become unstable
        Reduce Kp and Kd if your VRAM is limited as the total number of points per batch will be smaller
    */
    int Kp;             // default exptected_N / 200
    int Kd;             // default Kp * 5
    int low_pass_T;     // default 100
    int sample_batches; // default 999, number of batches per epoch
};

/**
 * @brief Create a networking using KPConv for segmenting point clouds
 * @param model_path Path to the model file exported via torch.jit.save
 * @return A KPConv network
 */
EXPORT SegmentHandle segment_create_kpconv_network(const char* model_path);

/**
 * @brief Configure a KPConv network
 * @param network The network to configure
 * @param config The configuration to use, if NULL the default configuration is used
 */
EXPORT void segment_configure_kpconv_network(SegmentHandle network, SegmentKPConvConfig const* config);

/* ================================ RandLANet ============================= */

struct SegmentRandLANetConfig {
    SegmentNetworkConfig base;
};

/**
 * @brief Create a networking using RandLANet for segmenting point clouds
 * @param model_path Path to the model file exported via torch.jit.save
 * @return A RandLA network
 */
EXPORT SegmentHandle segment_create_randla_network(const char* model_path);

/**
 * @brief Configure a RandLA network
 * @param network The network to configure
 * @param config The configuration to use, if NULL the default configuration is used
 */
EXPORT void segment_configure_randla_network(SegmentHandle network, SegmentRandLANetConfig const* config);

#ifdef __cplusplus
}
#endif
