/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include "segment.h"

#include <QLibrary>
#include <ccLog.h>

#if defined(_WIN32)
#    define WIN32_LEAN_AND_MEAN
#    include <windows.h>
#else
#    include <dlfcn.h>
#endif
#include <QFileInfo>
#include <iostream>

namespace util {
namespace dll {
inline void* load(QFile const& path)
{
#if defined(_WIN32)
    HMODULE dll = nullptr;

    QFileInfo fileInfo(path);
    SetDllDirectoryW(fileInfo.absolutePath().toStdWString().c_str());

    dll = LoadLibraryW(path.fileName().toStdWString().c_str());

    SetDllDirectoryW(nullptr);

    if (!dll) {
        DWORD error = GetLastError();

        if (error == ERROR_PROC_NOT_FOUND)
            return nullptr;

        char* message = nullptr;

        FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ALLOCATE_BUFFER,
            nullptr, error,
            MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
            (LPSTR)&message, 0, nullptr);

        ccLog::Error("LoadLibrary failed for %s: %s %i", path.fileName().toStdString().c_str(), message, error);

        if (message)
            LocalFree(message);
    }

    return dll;
#else
    void* res = nullptr;
    try {
        res = dlopen(path.fileName().toStdString().c_str(), RTLD_LAZY);

        if (!res)
            ccLog::Error("dlopen failed for %s: %%s", path.fileName().toStdString().c_str(), dlerror());
    } catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
    }

    return res;
#endif
}

inline void* sym(void* dll, const char* func)
{
    void* handle;

#if defined(_WIN32)
    handle = (void*)GetProcAddress((HMODULE)dll, func);
#else
    handle = dlsym(dll, func);
#endif
    return handle;
}

inline void unload(void* dll)
{
#if defined(_WIN32)
    FreeLibrary((HMODULE)dll);
#else
    if (dll)
        dlclose(dll);
#endif
}
}
}

class LibSegment {
    void* lib {};
    segment_is_gpu_available_t is_gpu_available {};
    segment_free_network_t free_network {};
    segment_initialize_t initialize_network {};

    segment_segment_points_t segment_points {};
    segment_segment_points_with_rgb_t segment_points_with_rgb {};
    segment_fetch_log_t fetch_log {};
    segment_get_version_info_t get_version_info {};
    segment_fetch_progress_t fetch_progress {};
    segment_interrupt_segmentation_t interrupt_segmentation {};
    segment_free_t free {};

    segment_configure_kpconv_network_t configure_kpconv_network {};
    segment_configure_randla_network_t configure_randla_network {};

    segment_create_kpconv_network_t create_kpconv_network {};
    segment_create_randla_network_t create_randla_network {};

    SegmentHandle network {};

public:
    LibSegment(QString const& path)
    {
        QFile f(path);
        lib = util::dll::load(f);
        if (!lib) {
            ccLog::Warning("[qSegment] Failed to load library: %s", path.toStdString().c_str());
        }

        is_gpu_available = (segment_is_gpu_available_t)util::dll::sym(lib, "segment_is_gpu_available");
        free_network = (segment_free_network_t)util::dll::sym(lib, "segment_free_network");
        initialize_network = (segment_initialize_t)util::dll::sym(lib, "segment_initialize_network");

        segment_points = (segment_segment_points_t)util::dll::sym(lib, "segment_segment_points");
        segment_points_with_rgb = (segment_segment_points_with_rgb_t)util::dll::sym(lib, "segment_segment_points_with_rgb");
        fetch_log = (segment_fetch_log_t)util::dll::sym(lib, "segment_fetch_log");
        get_version_info = (segment_get_version_info_t)util::dll::sym(lib, "segment_get_version_info");
        fetch_progress = (segment_fetch_progress_t)util::dll::sym(lib, "segment_fetch_progress");
        interrupt_segmentation = (segment_interrupt_segmentation_t)util::dll::sym(lib, "segment_interrupt_segmentation");
        free = (segment_free_t)util::dll::sym(lib, "segment_free");

        configure_kpconv_network = (segment_configure_kpconv_network_t)util::dll::sym(lib, "segment_configure_kpconv_network");
        configure_randla_network = (segment_configure_randla_network_t)util::dll::sym(lib, "segment_configure_randla_network");

        create_kpconv_network = (segment_create_kpconv_network_t)util::dll::sym(lib, "segment_create_kpconv_network");
        create_randla_network = (segment_create_randla_network_t)util::dll::sym(lib, "segment_create_randla_network");
    }

    ~LibSegment()
    {
        interruptSegmentation();
        unloadModel();
        util::dll::unload(lib);
    }

    bool loaded() const
    {
        return !!lib;
    }

    bool network_loaded() const
    {
        return network != nullptr;
    }

    void unloadModel()
    {
        if (network && free_network) {
            free_network(network);
        }
    }

    void free_memory(void* data)
    {
        if (free)
            free(data);
    }

    void loadKPConvNetwork(QString const& path)
    {
        if (create_kpconv_network) {
            network = create_kpconv_network(path.toStdString().c_str());
            if (network) {
                auto err = initialize_network(network);
                if (err != SEGMENT_OK)
                    ccLog::Error("Failed to initialize network: %i", err);
            }
        }
    }

    void loadRandLANetwork(QString const& path)
    {
        if (create_randla_network) {
            network = create_randla_network(path.toStdString().c_str());
            if (network) {
                auto err = initialize_network(network);
                if (err != SEGMENT_OK)
                    ccLog::Error("Failed to initialize network: %i", err);
            }
        }
    }

    void configureKPConvNetwork(SegmentKPConvConfig const* params)
    {
        if (configure_kpconv_network && network) {
            configure_kpconv_network(network, params);
        }
    }

    void configureRandLANetwork(SegmentRandLANetConfig const* params)
    {
        if (configure_randla_network && network) {
            configure_randla_network(network, params);
        }
    }

    void segmentPointCloud(float const* points, float const* color, uint64_t num_points, uint8_t** out_segmentation)
    {
        if (segment_points && network) {
            auto result = segment_points_with_rgb(network, points, color, num_points, out_segmentation);
            if (result != SEGMENT_OK) {
                ccLog::Error("[qSegment] Segmentation failed. %i", result);
            }
        }
    }

    void segmentPointCloud(float const* points, uint64_t num_points, uint8_t** out_segmentation)
    {
        if (segment_points && network) {
            auto result = segment_points(network, points, num_points, out_segmentation);
            if (result != SEGMENT_OK) {
                ccLog::Error("[qSegment] Segmentation failed. %i", result);
            }
        }
    }

    void interruptSegmentation()
    {
        if (interrupt_segmentation && network) {
            interrupt_segmentation(network);
        }
    }

    QString getLibraryInfo()
    {
        if (!get_version_info) {
            return "No version info available";
        }

        SegmentVersionInfo info;
        get_version_info(&info);

        auto libver = QString("v%1.%2.%3")
                          .arg(info.library_version.major)
                          .arg(info.library_version.minor)
                          .arg(info.library_version.patch);

        auto torchver = QString("v%1.%2.%3")
                            .arg(info.torch_version.major)
                            .arg(info.torch_version.minor)
                            .arg(info.torch_version.patch);

        QString computever;

        switch (info.compute_platform) {
        default:
        case SEGMENT_COMPUTE_CPU:
            computever = QString("CPU v%1.%2.%3")
                             .arg(info.compute_version.major)
                             .arg(info.compute_version.minor)
                             .arg(info.compute_version.patch);
            break;
        case SEGMENT_COMPUTE_CUDA:
            computever = QString("CUDA v%1.%2.%3")
                             .arg(info.compute_version.major)
                             .arg(info.compute_version.minor)
                             .arg(info.compute_version.patch);
            break;
        case SEGMENT_COMPUTE_ROCM:
            computever = QString("ROCM v%1.%2.%3")
                             .arg(info.compute_version.major)
                             .arg(info.compute_version.minor)
                             .arg(info.compute_version.patch);
            break;
        }

        return QString("LibSegment %1, LibTorch %2, %3")
            .arg(libver)
            .arg(torchver)
            .arg(computever);
    }

    QString getLog()
    {
        if (fetch_log && network) {
            auto log = fetch_log(network);
            if (log) {
                auto result = QString::fromUtf8(log);
                free(log);
                return result;
            }
        }
        return QString();
    }
};
