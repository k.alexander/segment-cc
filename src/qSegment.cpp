/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "qSegment.h"

// Dialog
#include "ccSegmentDlg.h"

// Qt
#include <QApplication>
#include <QMainWindow>
#include <QtConcurrentRun>
#include <QtGui>

// qCC_db
#include <ccCone.h>
#include <ccCylinder.h>
#include <ccGenericMesh.h>
#include <ccGenericPointCloud.h>
#include <ccPlane.h>
#include <ccPointCloud.h>
#include <ccProgressDialog.h>
#include <ccSphere.h>
#include <ccTorus.h>

// CCCoreLib
#include <CCPlatform.h>
#include <ScalarField.h>

#include <algorithm>

static ccMainAppInterface* s_app = nullptr;

qSegment::qSegment(QObject* parent /*=nullptr*/)
    : QObject(parent)
    , ccStdPluginInterface(":/CC/plugin/qSegment/info.json")
    , m_action(nullptr)
{
    s_app = m_app;
}

void qSegment::onNewSelection(const ccHObject::Container& selectedEntities)
{
    if (m_action)
        m_action->setEnabled(selectedEntities.size() == 1 && selectedEntities[0]->isA(CC_TYPES::POINT_CLOUD));
}

QList<QAction*> qSegment::getActions()
{
    // default action
    if (!m_action) {
        m_action = new QAction(getName(), this);
        m_action->setToolTip(getDescription());
        m_action->setIcon(getIcon());
        // connect signal
        connect(m_action, &QAction::triggered, this, &qSegment::doAction);
    }

    return QList<QAction*> { m_action };
}

void qSegment::registerCommands(ccCommandLineInterface* cmd)
{
    if (!cmd) {
        assert(false);
        return;
    }
}

void qSegment::doAction()
{
    assert(m_app);
    if (!m_app)
        return;

    const ccHObject::Container& selectedEntities = m_app->getSelectedEntities();
    size_t selNum = selectedEntities.size();
    if (selNum != 1) {
        ccLog::Error("[qSegment] Select only one cloud!");
        return;
    }

    ccHObject* ent = selectedEntities[0];
    assert(ent);
    if (!ent || !ent->isA(CC_TYPES::POINT_CLOUD)) {
        ccLog::Error("[qSegment] Select a real point cloud!");
        return;
    }
    ccPointCloud* pc = static_cast<ccPointCloud*>(ent);

    // input cloud
    CCVector3 bbMin, bbMax;
    pc->getBoundingBox(bbMin, bbMax);
    CCVector3 diff = bbMax - bbMin;
    float scale = std::max(std::max(diff[0], diff[1]), diff[2]);

    // init dialog with default values
    ccSegmentDlg* dlg = new ccSegmentDlg(pc, m_app->getMainWindow());

    if (pc->hasColors()) {
        dlg->rb_randla->setEnabled(true);
    } else {
        ccLog::Warning("[qSegment] Cloud does not have color information, RandLA-Net cannot be used.");
        dlg->rb_randla->setEnabled(false);
        dlg->rb_kpconv->toggled(true);
    }

    if (!dlg->exec()) {
        return;
    }
}
