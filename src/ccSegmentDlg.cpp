/*
 * Copyright (c) 2024, Alexander Kozel <alexander@kozel.cc>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ccSegmentDlg.h"

#include <ccLog.h>
#include <ccScalarField.h>
#include <thread>

ccSegmentDlg::ccSegmentDlg(ccPointCloud* pc, QWidget* parent)
    : QDialog(parent, Qt::Tool)
    , Ui::SegmentDialog()
    , m_pc(pc)
{
    setupUi(this);

    connect(btn_start, &QPushButton::clicked, this, &ccSegmentDlg::startSegmentation);
    connect(btn_stop, &QPushButton::clicked, this, &ccSegmentDlg::cancelSegmentation);

    connect(btn_load, &QPushButton::clicked, this, &ccSegmentDlg::loadLibrary);
    connect(btn_unload, &QPushButton::clicked, this, &ccSegmentDlg::unloadLibrary);

    txt_lib_path->setText("/run/media/office/Storage/git/ma/build/library/tests/libsegment.so");
    txt_model_path->setText("/run/media/office/Storage/git/ma/data/randlanet_toronto3d_jit_cpu.pt");

    connect(btn_load_model, &QPushButton::clicked, this, &ccSegmentDlg::loadModel);
    connect(btn_unload_model, &QPushButton::clicked, this, &ccSegmentDlg::unloadModel);

    connect(&m_tick_timer, &QTimer::timeout, this, &ccSegmentDlg::updateTick);

    connect(rb_kpconv, &QRadioButton::toggled, this, [this](bool on) {
        toggleKPConvSettings(on);
    });
}

void ccSegmentDlg::saveSettings() { }

void ccSegmentDlg::toggleKPConvSettings(bool on)
{
    sb_kd->setEnabled(on);
    sb_kp->setEnabled(on);
    sb_lowpass->setEnabled(on);
    sb_smoothing->setEnabled(on);
    sb_radius_ratio->setEnabled(on);
    sb_n->setEnabled(on);
    sb_lowpass->setEnabled(on);
    sb_sample_batches->setEnabled(on);
}

void ccSegmentDlg::startSegmentation()
{
    if (!m_lib) {
        ccLog::Error("[qSegment] Library not loaded");
        return;
    }

    btn_start->setEnabled(false);

    m_base_settings.num_fetch_threads = sb_threads->value();
    m_base_settings.num_iterations = sb_iterations->value();
    m_base_settings.validation_size = sb_validation_size->value();
    m_base_settings.prefer_gpu_inference = cb_use_gpu->isChecked();

    if (rb_kpconv->isChecked()) {
        m_kpconv_config.base = m_base_settings;
        m_kpconv_config.Kd = sb_kd->value();
        m_kpconv_config.Kp = sb_kp->value();
        m_kpconv_config.expected_N = sb_n->value();
        m_kpconv_config.low_pass_T = sb_lowpass->value();
        m_kpconv_config.sample_batches = sb_sample_batches->value();
        m_kpconv_config.test_radius_ratio = sb_radius_ratio->value();
        m_kpconv_config.test_smooth = sb_smoothing->value();

        m_lib->configureKPConvNetwork(&m_kpconv_config);
    } else {
        SegmentRandLANetConfig cfg {};
        cfg.base = m_base_settings;

        m_lib->configureRandLANetwork(&cfg);
    }

    auto colors = std::make_shared<std::vector<float>>();
    auto points = std::make_shared<std::vector<float>>();
    if (m_pc->hasColors()) {
        for (size_t i = 0; i < m_pc->size(); ++i) {
            const CCVector3* p = m_pc->getPoint(i);
            points->push_back(p->x);
            points->push_back(p->y);
            points->push_back(p->z);
            auto col = m_pc->getPointScalarValueColor(i);
            colors->push_back(col->r / 255.f);
            colors->push_back(col->g / 255.f);
            colors->push_back(col->b / 255.f);
        }
    } else {
        for (size_t i = 0; i < m_pc->size(); ++i) {
            const CCVector3* p = m_pc->getPoint(i);
            points->push_back(p->x);
            points->push_back(p->y);
            points->push_back(p->z);
        }
    }

    m_segment_thread = std::thread([this, colors, points]() {
        static_assert(sizeof(CCVector3) == sizeof(float) * 3, "ccPoint size mismatch");

        if (m_pc->hasColors())
            m_lib->segmentPointCloud(points->data(), colors->data(), m_pc->size(), &m_segment_result);
        else
            m_lib->segmentPointCloud(points->data(), m_pc->size(), &m_segment_result);

        if (m_segment_result) {
            ccLog::Print("[qSegment] Segmentation done");
            m_segmentation_done = true;
        }
    });

    m_tick_timer.start(500);
}

void ccSegmentDlg::updateTick()
{
    if (m_segmentation_done) {
        m_tick_timer.stop();
        btn_start->setEnabled(true);
        setWindowFlag(Qt::WindowCloseButtonHint, true);

        auto idx = m_pc->addScalarField("Segmentation");
        m_pc->setCurrentInScalarField(idx);
        auto* sf = m_pc->getCurrentInScalarField();

        for (size_t i = 0; i < m_pc->size(); ++i) {
            sf->setValue(i, (ScalarType)m_segment_result[i]);
        }
        sf->setValue(0, 0); // idk how else to force the minimum of the scale to be 0c
        m_lib->free_memory(m_segment_result);
        m_segment_result = nullptr;

        sf->computeMinAndMax();

        m_pc->setCurrentDisplayedScalarField(idx);
    }

    auto log = m_lib->getLog();
    if (!log.isEmpty()) {
        ccLog::Print("[qSegment] %s", log.toStdString().c_str());
    }
}

void ccSegmentDlg::cancelSegmentation()
{
    if (m_lib) {
        m_lib->interruptSegmentation();
    }
}

void ccSegmentDlg::loadLibrary()
{
    qApp->setOverrideCursor(Qt::WaitCursor);

    m_lib = std::make_unique<LibSegment>(txt_lib_path->text());

    if (m_lib->loaded()) {
        ccLog::Print("[qSegment] %s loaded successfully", m_lib->getLibraryInfo().toStdString().c_str());
        gb_model->setEnabled(true);
    }

    // restore cursor
    qApp->restoreOverrideCursor();
}

void ccSegmentDlg::unloadLibrary()
{
    m_lib = nullptr;
    gb_model->setEnabled(false);
    gb_parameters->setEnabled(false);
    btn_start->setEnabled(false);
    btn_stop->setEnabled(false);
}

void ccSegmentDlg::loadModel()
{
    if (!m_lib) {
        ccLog::Error("[qSegment] Library not loaded");
        return;
    }

    if (rb_kpconv->isChecked())
        m_lib->loadKPConvNetwork(txt_model_path->text());
    else
        m_lib->loadRandLANetwork(txt_model_path->text());

    if (m_lib->network_loaded()) {
        ccLog::Print("[qSegment] Model loaded successfully");
        gb_parameters->setEnabled(true);
        btn_start->setEnabled(true);
    }
}

void ccSegmentDlg::unloadModel()
{
    if (m_lib) {
        m_lib->unloadModel();
    }

    gb_parameters->setEnabled(false);
    btn_start->setEnabled(false);
    btn_stop->setEnabled(false);
}
